# Experience SpringCloud
SpringCloud初体验，边学边记录。

- es-spring
  - spring基础项目
  - spring三大核心组件（Bean、Context、Core）
 
- es-springmvc
  - 基于spring-webmvc的Web项目
  
- es-multi-module
  - 基于maven的多模块项目

- es-springboot
  - 基于springboot的项目

- es-springcloud
  - 基于springcloud的项目