package cn.ryanpenn.es.spring;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.assertEquals;

/**
 * HelloWorldServiceTest
 */
public class HelloWorldServiceTest {

    ApplicationContext context = null;

    @Before
    public void init() {
        context = new ClassPathXmlApplicationContext("applicationContext.xml");
    }

    @Test
    public void testSayHello() {
        HelloWorldService service = (HelloWorldService) context.getBean("helloWorldService");
        String greeting = service.sayHello("Ryan");
        assertEquals("Hello Ryan", greeting);
    }

}
