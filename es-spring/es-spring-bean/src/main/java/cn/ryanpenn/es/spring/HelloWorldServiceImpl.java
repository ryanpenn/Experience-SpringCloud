package cn.ryanpenn.es.spring;

/**
 * HelloWorldServiceImpl
 */
public class HelloWorldServiceImpl implements HelloWorldService {
    @Override
    public String sayHello(String name) {
        return "Hello " + name;
    }
}
