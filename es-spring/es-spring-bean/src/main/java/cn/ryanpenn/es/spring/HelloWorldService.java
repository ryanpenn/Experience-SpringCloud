package cn.ryanpenn.es.spring;

/**
 * HelloWorldService
 */
public interface HelloWorldService {

    String sayHello(String name);
}
