package cn.ryanpenn.es.spring;

/**
 * 普通Java对象
 *
 * @author pennryan
 */
public class SampleJavaBean {

    private String city;

    private int count;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "SampleJavaBean{" +
                "city='" + city + '\'' +
                ", count=" + count +
                '}';
    }
}

