package cn.ryanpenn.es.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Main
 *
 * @author ryan
 */
public class Main {

    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        HelloWorldService service = (HelloWorldService) context.getBean("helloWorldService");
        String greeting = service.sayHello("Ryan");
        System.out.println(greeting);

        SampleJavaBean bean = context.getBean(SampleJavaBean.class);
        System.out.println(bean);
    }
}
