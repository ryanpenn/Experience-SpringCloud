package cn.ryanpenn.es.spring;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.assertTrue;

/**
 * JdbcTest
 */
public class JdbcTest {

    ApplicationContext context;

    @Before
    public void init(){
        context = new ClassPathXmlApplicationContext("appContext.xml");
    }

    @Test
    public void testJdbc(){
        JdbcProgram.test();
    }

    @Test
    public void testSpringJdbc(){
        SpringJdbcProgram.test();
    }

    @Test
    public void testDao(){
        SpringDao dao = context.getBean(SpringDao.class);
        assertTrue(dao.create());

        Person p = dao.getById(1);
        System.out.println(p);
    }
}
