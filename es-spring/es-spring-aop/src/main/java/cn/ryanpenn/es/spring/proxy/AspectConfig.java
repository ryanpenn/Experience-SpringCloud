package cn.ryanpenn.es.spring.proxy;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.DeclareParents;
import org.springframework.stereotype.Component;

/**
 * AspectConfig
 *
 * @author pennryan
 */
@Component
@Aspect
public class AspectConfig {

    /**
     * 通过切面引入新功能
     */
    @DeclareParents(value = "cn.ryanpenn.es.spring.proxy.UserService",
            defaultImpl = cn.ryanpenn.es.spring.proxy.BasicVerifier.class)
    public Verifier verifier;
}
