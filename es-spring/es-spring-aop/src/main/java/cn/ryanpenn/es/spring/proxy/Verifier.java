package cn.ryanpenn.es.spring.proxy;

/**
 * Verifier
 *
 * @author pennryan
 */
public interface Verifier {

    /**
     * validate
     *
     * @param o (用于测试)
     * @return true|false
     */
    boolean validate(Object o);
}
