package cn.ryanpenn.es.spring;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * AnnotationMonitor
 *
 * @author pennryan
 */
@Component
@Aspect
public class AnnotationMonitor {

    @Before("execution(* cn.ryanpenn.es.spring.Subject.*(..))")
    public void printMessage(JoinPoint jp) {
        Signature sign = jp.getSignature();
        System.out.println(" 前置通知 beforeAdvice ... " + sign.getName()
                + " params count... " + jp.getArgs().length);
    }
}
