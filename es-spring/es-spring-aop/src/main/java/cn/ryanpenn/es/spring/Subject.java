package cn.ryanpenn.es.spring;

import org.springframework.stereotype.Component;

/**
 * Subject
 *
 * @author pennryan
 */
@Component
public class Subject {

    public void doSomething(String msg) {
        System.out.println("Subject doSomething with " + msg);
    }

    public void sayHi() {
        System.out.println("Subject say Hi.");
    }

}
