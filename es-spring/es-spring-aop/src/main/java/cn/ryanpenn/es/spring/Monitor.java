package cn.ryanpenn.es.spring;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Monitor
 *
 * @author pennryan
 */
public class Monitor {

    public void printTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
        System.out.println("CurrentTime = " + sdf.format(new Date()));
    }

}
