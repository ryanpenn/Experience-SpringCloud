package cn.ryanpenn.es.spring.proxy;

/**
 * BasicVerifier
 *
 * @author pennryan
 */
public class BasicVerifier implements Verifier {
    @Override
    public boolean validate(Object o) {
        return true;
    }
}
