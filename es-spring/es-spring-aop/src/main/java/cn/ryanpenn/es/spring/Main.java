package cn.ryanpenn.es.spring;

import cn.ryanpenn.es.spring.proxy.UserService;
import cn.ryanpenn.es.spring.proxy.Verifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Main
 *
 * @author pennryan
 */
public class Main {

    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("appContext.xml");
        Subject subject = (Subject) context.getBean("subject");
        subject.doSomething("Hello");
        subject.sayHi();

        UserService s = context.getBean(UserService.class);
        // 虽然UserService并未实现Verifier接口,但可以将UserService转化为Verifier接口类型
        Verifier v = (Verifier) s;
        if (v.validate(null)) {
            System.out.println("验证成功");
        }
    }
}
