package cn.ryanpenn.es.spring.model;

import java.io.Serializable;

/**
 * Hello
 *
 * @author pennryan
 */
public class Hello implements Serializable {

    int id;

    String from;

    String to;

    String message;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
