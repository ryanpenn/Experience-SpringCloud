package cn.ryanpenn.es.spring.service.impl;

import cn.ryanpenn.es.spring.dao.HelloDao;
import cn.ryanpenn.es.spring.model.Hello;
import cn.ryanpenn.es.spring.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * HelloServiceImpl
 *
 * @author pennryan
 */
@Service
public class HelloServiceImpl implements HelloService {

    @Autowired
    private HelloDao helloDao;

    @Override
    public Hello sayHello(String from, String to, String message) {
        Hello hello = new Hello();
        hello.setFrom(from);
        hello.setTo(to);
        hello.setMessage(message);
        helloDao.add(hello);
        return hello;
    }

    @Override
    public List<Hello> getHistory(String user) {
        return helloDao.query(user);
    }

}
