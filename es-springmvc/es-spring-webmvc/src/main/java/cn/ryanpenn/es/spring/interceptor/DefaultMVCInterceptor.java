package cn.ryanpenn.es.spring.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 默认拦截器
 */
public class DefaultMVCInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath();
		request.setAttribute("basePath", basePath);

		if ((handler instanceof HandlerMethod) == false) {
			return super.preHandle(request, response, handler);
		}

		return true;
	} 
}
