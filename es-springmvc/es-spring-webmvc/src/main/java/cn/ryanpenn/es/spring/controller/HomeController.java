package cn.ryanpenn.es.spring.controller;

import cn.ryanpenn.es.spring.model.Hello;
import cn.ryanpenn.es.spring.service.HelloService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * HomeController
 *
 * @author pennryan
 */
@Controller
public class HomeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    HelloService helloService;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello(@RequestParam(required = false) String from,
                        @RequestParam(required = false) String to,
                        @RequestParam(required = false) String msg,
                        Model model) {
        LOGGER.info("/hello");

        // 给Service传参
        Hello hello = helloService.sayHello(from, to, msg);
        // 为View设置model
        model.addAttribute("hello", hello);
        model.addAttribute("greeting", "Hello world.");
        // 返回View
        return "index";
    }

    @RequestMapping(value = "/iframe", method = RequestMethod.GET)
    public String iframeView(Model model) {
        LOGGER.info("/iframe");
        model.addAttribute("greeting", "this is view1.");
        return "inner_view";
    }

    @RequestMapping(value = "/content", method = RequestMethod.GET)
    public String innerView(Model model) {
        LOGGER.info("/content");
        model.addAttribute("greeting", "this is view2.");
        return "content";
    }
}
