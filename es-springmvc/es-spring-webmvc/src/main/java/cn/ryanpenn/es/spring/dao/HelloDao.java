package cn.ryanpenn.es.spring.dao;

import cn.ryanpenn.es.spring.model.Hello;

import java.util.List;
import java.util.Map;

/**
 * HelloDao
 *
 * @author pennryan
 */
public interface HelloDao {

    int add(Hello hello);

    boolean update(Hello hello);

    boolean delete(int id);

    Hello getById(int id);

    List<Hello> query(String user);
}
