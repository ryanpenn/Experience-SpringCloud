package cn.ryanpenn.es.spring.service;

import cn.ryanpenn.es.spring.model.Hello;

import java.util.List;

/**
 * HelloService
 *
 * @author pennryan
 */
public interface HelloService {

    /**
     * sayHello
     */
    Hello sayHello(String from,String to,String message);

    List<Hello> getHistory(String user);

}
