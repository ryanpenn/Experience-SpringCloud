var SmartJS = window.SmartJS || {};

SmartJS.SmartIframe = new function(){
    var self = this;
    var active_iframe = null;

    self.sayHello = function(){
        return "hello";
    };

    self.init = function(){
        if(document.attachEvent) {
            window.attachEvent("onresize",  function(){
                self.autoFitForContent(active_iframe);
            });
        }
        else {
            window.addEventListener('resize',  function(){
                self.autoFitForContent(active_iframe);
            },  false);
        }
    };

    self.createIframe = function(id, src, divContainer){
        if(divContainer){
            var myiframe = document.createElement("iframe");
            myiframe.style.width="100%";
            myiframe.style.height="0px";
            myiframe.style.float="left";
            myiframe.marginheight=0;
            myiframe.marginwidth=0;
            myiframe.frameborder=0;
            myiframe.setAttribute('frameborder', '0', 0);
            myiframe.scrolling="no";
            myiframe.id=id;
            myiframe.name=id;

            if(document.attachEvent) {
                myiframe.attachEvent("onload",  function(){
                    self.autoFitForContent(myiframe);
                });
            }
            else {
                myiframe.addEventListener('load',  function(){
                    self.autoFitForContent(myiframe);
                },  false);
            }

            active_iframe = myiframe;
            myiframe.src=src;

            var innerDiv = document.createElement("div");
            innerDiv.appendChild(myiframe);

            divContainer.appendChild(innerDiv);

            return myiframe;
        }
    };

    self.activeiFrame = function(ifr){
        if(ifr){
            active_iframe = ifr;
            self.autoFitForContent(ifr);
        }
    };

    self.autoFitForContent = function(_ifr) {
        var ifr = _ifr;

        if(ifr){
            var ex;
            try {
                //var h= calcPageHeight(ifr.contentWindow.document);
                //if(document.all) {h += 4;}
                //if(window.opera) {h += 1;}
                //_ifr.style.height = h+"px";
                ifr.style.height = 0;
                ifr.style.height = ifr.contentWindow.document.body.scrollHeight + 'px';

                //if(ifr.contentWindow != parent) {
                //    var a = parent.document.getElementsByTagName("IFRAME");
                //    for(var i=0; i<a.length; i++) {
                //        if(a[i].contentWindow==ifr.contentWindow) {
                //            var h= calcPageHeight(ifr.contentWindow.document);
                //            if(document.all) {h += 4;}
                //            if(window.opera) {h += 1;}
                //            a[i].style.height = "0px";
                //            a[i].style.height = h +"px";
                //        }
                //    }
                //}
            }
            catch (ex){
                console.log(ex);
            }
        }
    };

};
