<html lang="zh-cn">

<head>
    <meta charset="utf-8"/>
    <title>Sample</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="sample web" name="description"/>
    <meta content="sample.com" name="author"/>

    <!-- Begin GLOBAL CSS -->
    <link href="${basePath}/static/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="${basePath}/static/vendor/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL CSS -->

    <!-- Begin PRIVATE CSS -->
    <link href="${basePath}/static/assets/css/main.css" rel="stylesheet" type="text/css"/>
    <!-- END PRIVATE CSS -->

    <!-- Begin PAGE CSS -->
    <style type="text/css">

    </style>
    <!-- END PAGE CSS -->

    <link rel="shortcut icon" href="${basePath}/static/assets/img/favicon.ico"/>
</head>
<body>
    <div class="container">
        <div class="text-info">${greeting}</div>
        <div id="view1_div">
            <iframe src="${basePath}/iframe" frameborder="no" style="width:150px; height:100px; float:left;"></iframe>
        </div>
        <div id="view2_div"></div>
    </div>

<!-- Begin GLOBAL JS -->
<script src="${basePath}/static/vendor/jquery/jquery-2.2.3.min.js" type="text/javascript"></script>
<script src="${basePath}/static/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- END GLOBAL JS -->

<!-- Begin PRIVATE JS -->
<script src="${basePath}/static/assets/js/smart_iframe.js" type="text/javascript"></script>
<script src="${basePath}/static/assets/js/main.js" type="text/javascript"></script>
<!-- END PRIVATE JS -->

<!-- Begin PAGE JS -->
<script type="text/javascript" language="JavaScript">

    $(window).ready(function(){
        SmartJS.SmartIframe.init();
    });

    function createIframe(){
        var container = document.getElementById("view2_div")
        var ifr = SmartJS.SmartIframe.createIframe("iframepage", "${basePath}/content" ,container)
    }

</script>
<!-- END PAGE JS -->

</body>
</html>
