<html lang="zh-cn">

<head>
    <meta charset="utf-8"/>
    <title>Sample</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="sample web" name="description"/>
    <meta content="sample.com" name="author"/>

    <!-- Begin GLOBAL CSS -->
    <link href="${basePath}/static/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="${basePath}/static/vendor/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL CSS -->

    <!-- Begin PRIVATE CSS -->
    <link href="${basePath}/static/assets/css/main.css" rel="stylesheet" type="text/css"/>
    <!-- END PRIVATE CSS -->

    <!-- Begin PAGE CSS -->
    <style type="text/css">

    </style>
    <!-- END PAGE CSS -->

    <link rel="shortcut icon" href="${basePath}/static/assets/img/favicon.ico"/>
</head>
<body>
<div class="container">

    <a href="javascript:;" onclick="openNewFrame()"> ${greeting} </a>

</div>

<!-- Begin GLOBAL JS -->
<script src="${basePath}/static/vendor/jquery/jquery-2.2.3.min.js" type="text/javascript"></script>
<script src="${basePath}/static/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- END GLOBAL JS -->

<!-- Begin PRIVATE JS -->
<script src="${basePath}/static/assets/js/main.js" type="text/javascript"></script>
<!-- END PRIVATE JS -->

<!-- Begin PAGE JS -->
<script type="text/javascript" language="JavaScript">

    function openNewFrame() {
        parent.createIframe();
    }

</script>
<!-- END PAGE JS -->

</body>
</html>
