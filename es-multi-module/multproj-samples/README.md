# 多级多模块maven工程示例

- 根模块（multproj-samples）的pom.xml 中用来统一个子模块中使用的第三方依赖包的版本.
- Web子模块（sbp-web）的pom.xml 用来管理两个Web子项目: 管理后台（sbp-admin）为springboot项目,前端站点（sbp-site）为普通Web项目.
