package cn.ryanpenn.es.springboot;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * IndexControllerTest
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RestApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration
public class IndexControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void getHello() throws Exception {
        Map<String, String> multiValueMap = new HashMap<>();
        multiValueMap.put("name", "nick");
        String result = testRestTemplate.getForObject("/index/hello?name={name}", String.class, multiValueMap);
        Assert.assertEquals(result, "Hello nick");
    }

    @Test
    public void getMessage() throws Exception {
        Message msg = testRestTemplate.getForObject("/index/message/1", Message.class);
        System.out.println(msg);
        Assert.assertEquals(msg.getCode(), 1);
    }

}
