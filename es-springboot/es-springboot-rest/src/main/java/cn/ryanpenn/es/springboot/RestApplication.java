package cn.ryanpenn.es.springboot;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * RestApplication
 *
 * @author pennryan
 */
@SpringBootApplication
public class RestApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(RestApplication.class);
        application.setBannerMode(Banner.Mode.LOG);
        application.run(args);
    }
}
