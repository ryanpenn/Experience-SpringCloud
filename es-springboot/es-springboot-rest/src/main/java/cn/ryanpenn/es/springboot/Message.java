package cn.ryanpenn.es.springboot;

/**
 * Message
 */
public class Message {

    int code;
    String info;
    Object Data;

    public Message() {
    }

    public Message(int code, String info, Object data) {
        this.code = code;
        this.info = info;
        Data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Object getData() {
        return Data;
    }

    public void setData(Object data) {
        Data = data;
    }

    @Override
    public String toString() {
        return "Message{" +
                "code=" + code +
                ", info='" + info + '\'' +
                ", Data=" + Data +
                '}';
    }
}
