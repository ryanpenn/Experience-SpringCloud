package cn.ryanpenn.es.springboot;

import org.springframework.web.bind.annotation.*;

/**
 * IndexController
 */
@RestController
@RequestMapping("index")
public class IndexController {

    @GetMapping("/hello")
    public String sayHello(@RequestParam String name) {
        return "Hello " + name;
    }

    @GetMapping("/message/{code}")
    public Message message(@PathVariable int code) {
        Message msg = new Message(code, "hello", "String data");
        return msg;
    }
}
