package cn.ryanpenn.es.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * RestApplication
 */
@SpringBootApplication
public class RestApplication {

    /**
     * http://localhost:8080/swagger-ui.html
     */
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(RestApplication.class);
        application.run(args);
    }
}
