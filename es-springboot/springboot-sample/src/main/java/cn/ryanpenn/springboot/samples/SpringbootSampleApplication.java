package cn.ryanpenn.springboot.samples;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class SpringbootSampleApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(SpringbootSampleApplication.class, args);
		System.out.println("SpringbootSampleApplication start...");

		System.in.read();
		System.out.println("bye");
	}
}
