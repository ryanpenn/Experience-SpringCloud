package cn.ryanpenn.es.ms.report.controller;

import cn.ryanpenn.es.ms.report.model.WeatherResponse;
import cn.ryanpenn.es.ms.report.service.ApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

/**
 * ReportController
 *
 * @author pennryan
 */
@ApiIgnore
@Controller
public class ReportController {

    @Autowired
    ApiClient apiClient;

    /**
     * 天气预报页面
     *
     * @param code  城市代码,如:101010100 北京
     * @param model 页面传参
     * @return weather视图
     */
    @RequestMapping(value = "site/{code}", method = RequestMethod.GET)
    public ModelAndView index(@PathVariable String code, Model model) {
        model.addAttribute("cityCode", code);
        model.addAttribute("cityList", apiClient.getCityList());
        WeatherResponse weather = apiClient.getByCityCode(code);
        if (weather != null) {
            model.addAttribute("weather", weather.getData());
        } else {
            model.addAttribute("weather", null);
        }
        return new ModelAndView("weather", "report", model);
    }

    @GetMapping("info")
    public String info() {
        return "redirect:/site/101010100";
    }
}
