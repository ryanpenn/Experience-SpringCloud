package cn.ryanpenn.es.ms.report.service;

import cn.ryanpenn.es.ms.report.model.City;
import cn.ryanpenn.es.ms.report.model.WeatherResponse;
import cn.ryanpenn.es.ms.report.service.fallback.ApiFallback;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * ApiClient
 *
 * @author pennryan
 */
@FeignClient(value = "ms-weather-api-gateway",fallback = ApiFallback.class)
public interface ApiClient {

    /**
     * 获取城市列表
     *
     * @return 城市列表
     */
    @GetMapping(value = "city/list", produces = "application/json;charset=UTF-8")
    List<City> getCityList();

    /**
     * 根据城市代码查询天气
     *
     * @param code 城市代码
     * @return 天气数据
     */
    @GetMapping(value = "weather/{code}", produces = "application/json;charset=utf-8")
    WeatherResponse getByCityCode(@PathVariable("code") String code);
}
