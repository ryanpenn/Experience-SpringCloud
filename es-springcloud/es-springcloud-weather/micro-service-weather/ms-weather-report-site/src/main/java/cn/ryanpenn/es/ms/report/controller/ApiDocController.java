package cn.ryanpenn.es.ms.report.controller;

import cn.ryanpenn.es.ms.report.model.City;
import cn.ryanpenn.es.ms.report.model.WeatherResponse;
import cn.ryanpenn.es.ms.report.service.ApiClient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ApiDocController
 *
 * @author pennryan
 */
@Api("天气预报API")
@RestController
@RequestMapping("api")
public class ApiDocController {

    @Autowired
    ApiClient apiClient;

    /**
     * 获取城市列表
     *
     * @return 城市列表
     */
    @ApiOperation(value = "获取城市列表", notes = "城市列表")
    @GetMapping(value = "/city/list", produces = "application/json;charset=UTF-8")
    public List<City> getCityList() {
        return apiClient.getCityList();
    }

    /**
     * 根据城市代码查询天气
     *
     * @param code 城市代码
     * @return 天气数据
     */
    @ApiOperation(value = "根据城市代码查询天气", notes = "查询城市的天气数据")
    @ApiImplicitParam(name = "code", value = "城市代码", paramType = "path", required = true, dataType = "String")
    @GetMapping(value = "/weather/{code}", produces = "application/json;charset=utf-8")
    public WeatherResponse getByCityCode(@PathVariable String code) {
        return apiClient.getByCityCode(code);
    }
}
