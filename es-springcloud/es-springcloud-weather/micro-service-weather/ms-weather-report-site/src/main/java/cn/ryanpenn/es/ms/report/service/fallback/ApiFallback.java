package cn.ryanpenn.es.ms.report.service.fallback;

import cn.ryanpenn.es.ms.report.model.City;
import cn.ryanpenn.es.ms.report.model.WeatherResponse;
import cn.ryanpenn.es.ms.report.service.ApiClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

/**
 * ApiFallback
 *
 * @author pennryan
 */
@Component
public class ApiFallback implements ApiClient {
    @Override
    public List<City> getCityList() {
        // 默认返回北京
        List<City> list = new ArrayList<>();
        list.add(new City("101010100", "北京"));
        return list;
    }

    @Override
    public WeatherResponse getByCityCode(@PathVariable("code") String code) {
        // 默认返回空
        return null;
    }
}
