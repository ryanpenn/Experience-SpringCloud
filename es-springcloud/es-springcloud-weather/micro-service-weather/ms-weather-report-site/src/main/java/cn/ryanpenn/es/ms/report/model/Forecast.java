package cn.ryanpenn.es.ms.report.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Forecast
 *
 * @author pennryan
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Forecast {

    String date;
    String high;
    String fengli;
    String low;
    String fengxiang;
    String type;

}
