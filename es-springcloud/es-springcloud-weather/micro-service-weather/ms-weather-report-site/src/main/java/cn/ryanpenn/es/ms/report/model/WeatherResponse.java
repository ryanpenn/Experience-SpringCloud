package cn.ryanpenn.es.ms.report.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * WeatherResponse
 *
 * @author pennryan
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WeatherResponse {

    Weather data;
    Integer status;
    String desc;

}
