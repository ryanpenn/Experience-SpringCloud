package cn.ryanpenn.es.ms.gathering.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * GatheringController
 *
 * @author pennryan
 */
@RestController
public class GatheringController {

    @GetMapping("info")
    public String info() {
        return "Gathering service is available!";
    }
}
