package cn.ryanpenn.es.ms.gathering.context;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * AppContext
 *
 * @author pennryan
 */
@Component
public class AppContext implements ApplicationContextAware {

    @Autowired
    JobDetail weatherDataSyncJobDetail;
    @Autowired
    Trigger weatherDataSyncJobTrigger;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        try {
            // 创建scheduler
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            weatherDataSyncJobDetail.getJobDataMap().put("context", applicationContext);
            scheduler.scheduleJob(weatherDataSyncJobDetail, weatherDataSyncJobTrigger);
            scheduler.start();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
}
