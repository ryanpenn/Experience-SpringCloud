package cn.ryanpenn.es.ms.gathering.service;

import cn.ryanpenn.es.ms.gathering.model.City;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * CityServiceClient
 *
 * @author pennryan
 */
@FeignClient("ms-city-service")
public interface CityServiceClient {

    /**
     * 获取城市列表
     * @return 城市列表
     */
    @GetMapping("list")
    List<City> getCityList();
}
