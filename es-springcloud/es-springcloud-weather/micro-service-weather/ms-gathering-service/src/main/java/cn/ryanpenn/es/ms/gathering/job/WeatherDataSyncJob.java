package cn.ryanpenn.es.ms.gathering.job;

import cn.ryanpenn.es.ms.gathering.service.GatheringService;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

/**
 * WeatherDataSyncJob
 *
 * @author pennryan
 */
@Component
public class WeatherDataSyncJob extends QuartzJobBean {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("Weather data sync job.");

        JobDataMap dataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        ApplicationContext context = (ApplicationContext) dataMap.get("context");
        if (context != null) {
            GatheringService gatheringService = context.getBean(GatheringService.class);
            if (gatheringService != null) {
                gatheringService.gatheringData();
            }
        }
    }
}
