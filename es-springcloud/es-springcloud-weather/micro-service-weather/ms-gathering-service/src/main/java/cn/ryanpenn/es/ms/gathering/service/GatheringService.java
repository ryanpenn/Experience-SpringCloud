package cn.ryanpenn.es.ms.gathering.service;

/**
 * GatheringService
 *
 * @author pennryan
 */
public interface GatheringService {

    /**
     * 收集数据
     */
    void gatheringData();
}
