package cn.ryanpenn.es.ms.weather.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Yesterday
 *
 * @author pennryan
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Yesterday {

    String date;
    String high;
    String fx;
    String low;
    String fl;
    String type;

}
