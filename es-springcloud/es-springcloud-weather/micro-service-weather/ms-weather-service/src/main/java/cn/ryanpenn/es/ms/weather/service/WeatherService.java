package cn.ryanpenn.es.ms.weather.service;

import cn.ryanpenn.es.ms.weather.model.WeatherResponse;

/**
 * WeatherService
 *
 * @author pennryan
 */
public interface WeatherService {

    /**
     * 根据城市代码查询天气
     * @param code 城市代码
     * @return 天气数据
     */
    WeatherResponse getByCityCode(String code);
}
