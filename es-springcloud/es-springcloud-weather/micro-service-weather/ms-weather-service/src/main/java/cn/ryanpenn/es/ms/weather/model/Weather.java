package cn.ryanpenn.es.ms.weather.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Weather
 *
 * @author pennryan
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Weather {

    String city;
    String aqi;
    String ganmao;
    String wendu;
    Yesterday yesterday;
    List<Forecast> forecast;

}
