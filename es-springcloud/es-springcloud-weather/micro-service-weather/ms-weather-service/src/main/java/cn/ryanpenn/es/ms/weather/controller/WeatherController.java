package cn.ryanpenn.es.ms.weather.controller;

import cn.ryanpenn.es.ms.weather.model.WeatherResponse;
import cn.ryanpenn.es.ms.weather.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * WeatherController
 *
 * @author pennryan
 */
@RestController
public class WeatherController {

    @Autowired
    WeatherService weatherService;

    @GetMapping("info")
    public String info() {
        return "Weather service is available.";
    }

    @RequestMapping(value = "/{code}", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    public WeatherResponse getByCityCode(@PathVariable String code) {
        return weatherService.getByCityCode(code);
    }


}
