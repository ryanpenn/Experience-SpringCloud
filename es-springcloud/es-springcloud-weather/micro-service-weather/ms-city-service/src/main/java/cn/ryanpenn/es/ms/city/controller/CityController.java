package cn.ryanpenn.es.ms.city.controller;

import cn.ryanpenn.es.ms.city.model.City;
import cn.ryanpenn.es.ms.city.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * CityController
 *
 * @author pennryan
 */
@RestController
public class CityController {

    @Autowired
    CityService cityService;

    @GetMapping(value = "list", produces = "application/json;charset=UTF-8")
    public List<City> getCityList(@RequestParam(required = false, defaultValue = "false")
                                          boolean reload) {
        try {
            return cityService.getCityList(reload);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

    @GetMapping("info")
    public String getInfo() {
        return "CityService is available!";
    }
}
