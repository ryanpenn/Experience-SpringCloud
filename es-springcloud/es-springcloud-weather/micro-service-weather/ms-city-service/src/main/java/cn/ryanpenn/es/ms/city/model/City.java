package cn.ryanpenn.es.ms.city.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * City
 *
 * @author pennryan
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class City {

    String code;
    String name;

}
