package cn.ryanpenn.es.ms.city.service;

import cn.ryanpenn.es.ms.city.model.City;

import java.io.IOException;
import java.util.List;

/**
 * CityService
 *
 * @author pennryan
 */
public interface CityService {

    /**
     * 获取城市列表
     * @param reload 是否重新加载
     * @return 城市列表
     */
    List<City> getCityList(boolean reload) throws IOException;
}
