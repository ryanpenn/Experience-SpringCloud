# micro-service-weather

## 模块说明
- 基于eureka的服务发现,ms-eureka-server,运行端口: 8000
- 城市微服务,ms-city-service,端口: 8100...8199
- 天气微服务,ms-weather-service,端口: 8200...8299
- 数据采集微服务,ms-gathering-service,端口: 8300...8399
- 天气API网关,ms-weather-api-gateway,端口: 8400...8499
- 天气预报站点,ms-weather-report-site,端口: 8500
- 模拟客户端,ms-weather-webapp, Vue.js+jQuery实现

## 项目说明
- ms-eureka-server
  > - 通过 eureka 实现的服务中心
  > - 依赖: spring-cloud-starter-netflix-eureka-server

- ms-city-service
  > - 城市列表服务提供者
  > - 通过 eureka 实现的微服务客户端
  > - 依赖: spring-cloud-starter-netflix-eureka-client

- ms-gathering-service
  > - 天气数据采集服务
  > - 通过 eureka 实现的微服务客户端
  > - 依赖:
  >   - ms-city-service（城市列表服务）
  >   - spring-cloud-starter-netflix-eureka-client
  >   - spring-cloud-starter-openfeign（服务消费者）
  >   - spring-boot-starter-data-redis（保存数据到Redis）
  >   - quartz（定时采集）

- ms-weather-service
  > - 天气预报服务提供者
  > - 通过 eureka 实现的微服务客户端
  > - 依赖:
  >   - spring-cloud-starter-netflix-eureka-client
  >   - spring-boot-starter-data-redis

- ms-weather-api-gateway
  > - 天气服务API网关
  > - 通过 eureka 实现的微服务客户端
  > - 依赖:
  >   - spring-cloud-starter-netflix-eureka-client
  >   - spring-cloud-starter-netflix-zuul

- ms-weather-report-site
  > - 天气预报网站
  > - 通过 eureka 实现的微服务客户端
  > - 依赖:
  >   - ms-weather-api-gateway（API网关）
  >   - spring-cloud-starter-netflix-eureka-client
  >   - spring-cloud-starter-openfeign
  >   - spring-cloud-starter-hystrix（断路器）
  >   - spring-boot-starter-thymeleaf（前端界面）

- ms-weather-webapp
  > - 天气预报Webpp（API同样适合Native客户端调用）
  > - 通过 Vue+jQuery 实现的Web单页面应用
  > - 依赖:
  >   - ms-weather-api-gateway（URL依赖）
  >   - Vue.js
  >   - jQuery

## 配置服务信息
- /info 查看
> 在 application.yml 中配置，可以从pom.xml中获取
```
info:  
  app:  
    name: "@project.name@"
    description: "@project.description@"  
    version: "@project.version@"  
    spring-boot-version: "@project.parent.version@" 
```

## 配置服务监控
- spring-boot actuator
  > - 引入该模块可以监控各项服务的性能
  > - 通过 /health 查看
  > - 配置了 Eureka 是， /health 可以查看服务相关信息
  > - 配置了 zuul 时，/routes 可以查看路由信息
```
<dependency>  
    <groupId>org.springframework.boot</groupId>  
    <artifactId>spring-boot-starter-actuator</artifactId>  
</dependency>
```

## 备注
> - 通过 [Steeltoe](http://steeltoe.io/),可以让 .net core 开发的项目也享受到 spring cloud 的强大功能
> - 参考示例： [spring cloud+.net core搭建微服务架构](http://www.cnblogs.com/longxianghui/tag/%E5%BE%AE%E6%9C%8D%E5%8A%A1/)
