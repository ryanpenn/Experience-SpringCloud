var app = new Vue({
    el: '#app',
    data: {
        // 访问API网关（若在不同于域名下,需在网关上设置允许跨域访问）
        cityApi:'http://localhost:8400/city',
        weatherApi:'http://localhost:8400/weather',
        title: '城市列表',
        report: {
            cityCode: -1,
            cityList: [],
            weather: null
        }
    },
    methods: {
        getCityList: function () {
            var that = this;
            $.ajax({
                url: app.$data.cityApi + "/list",
                success: function (result) {
                    app.$data.cityCode = result[0].code;
                    app.$data.report.cityList = result;
                    that.getWeather(result[0].code);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest.status);
                    console.log(XMLHttpRequest.readyState);
                    console.log(textStatus);
                }
            });
        },
        getWeather: function (cityCode) {
            var that = this;
            $.ajax({
                url: app.$data.weatherApi + "/" + cityCode,
                success: function (result) {
                    app.$data.report.weather = result.data;
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest.status);
                    console.log(XMLHttpRequest.readyState);
                    console.log(textStatus);
                }
            });
        },
        // 切换城市
        chooseCity: function (e) {
            console.log(e.target.value);
            this.getWeather(e.target.value);
        },
        init: function () {
            this.getCityList();
        }
    }
});