package cn.ryanpenn.es.springclound;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * WeatherApplication
 *
 * @author pennryan
 */
@SpringBootApplication
public class WeatherApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(WeatherApplication.class);
        application.run(args);
    }
}
