package cn.ryanpenn.es.springclound.config;

import cn.ryanpenn.es.springclound.job.WeatherDataSyncJob;
import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * QuartzConfig
 *
 * @author pennryan
 */
@Configuration
public class QuartzConfig {

    /**
     * 每隔30分钟同步一次数据,测试时可改为30秒
     */
    private final int intervalSeconds = 30 * 60;

    @Bean
    public JobDetail weatherDataSyncJobDetail() {
        return JobBuilder.newJob(WeatherDataSyncJob.class)
                .withIdentity("weatherDataSyncJob")
                .storeDurably()
                .build();
    }

    @Bean
    public Trigger weatherDataSyncJobTrigger() {
        ScheduleBuilder scheduleBuilder = SimpleScheduleBuilder.simpleSchedule()
                .withIntervalInSeconds(intervalSeconds)
                .repeatForever();

        return TriggerBuilder.newTrigger()
                .forJob(weatherDataSyncJobDetail())
                .withIdentity("weatherDataSyncJobTrigger")
                .withSchedule(scheduleBuilder)
                .build();
    }
}
