package cn.ryanpenn.es.springclound.model;

import java.util.List;

/**
 * Weather
 *
 * @author pennryan
 */
public class Weather {

    String city;
    String aqi;
    String ganmao;
    String wendu;
    Yesterday yesterday;
    List<Forecast> forecast;

    public Weather() {
    }

    public Weather(String city, String aqi, String ganmao, String wendu, Yesterday yesterday, List<Forecast> forecast) {
        this.city = city;
        this.aqi = aqi;
        this.ganmao = ganmao;
        this.wendu = wendu;
        this.yesterday = yesterday;
        this.forecast = forecast;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAqi() {
        return aqi;
    }

    public void setAqi(String aqi) {
        this.aqi = aqi;
    }

    public String getGanmao() {
        return ganmao;
    }

    public void setGanmao(String ganmao) {
        this.ganmao = ganmao;
    }

    public String getWendu() {
        return wendu;
    }

    public void setWendu(String wendu) {
        this.wendu = wendu;
    }

    public Yesterday getYesterday() {
        return yesterday;
    }

    public void setYesterday(Yesterday yesterday) {
        this.yesterday = yesterday;
    }

    public List<Forecast> getForecast() {
        return forecast;
    }

    public void setForecast(List<Forecast> forecast) {
        this.forecast = forecast;
    }
}
