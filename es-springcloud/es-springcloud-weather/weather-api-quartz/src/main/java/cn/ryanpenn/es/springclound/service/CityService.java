package cn.ryanpenn.es.springclound.service;

import java.io.IOException;
import java.util.Map;

/**
 * CityService
 *
 * @author pennryan
 */
public interface CityService {

    void load() throws IOException;

    Map<String, String> getCityData();
}
