package cn.ryanpenn.es.springclound.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * SwaggerConfig
 * http://localhost:8080/swagger-ui.html
 *
 * @author pennryan
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * 创建swagger2的配置
     *
     * @return
     */
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                // 扫描包路径
                .apis(RequestHandlerSelectors.basePackage("cn.ryanpenn.es.springclound.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    //构建 api文档的详细信息函数,注意这里的注解引用的是哪个

    /**
     * 构建api文档的详细信息函数
     *
     * @return
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                //页面标题
                .title("天气预报API")
                //创建人
                .contact(new Contact("Test", "http://www.weather.com", ""))
                //版本号
                .version("1.0")
                //描述
                .description("天气预报服务API")
                .build();
    }

}
