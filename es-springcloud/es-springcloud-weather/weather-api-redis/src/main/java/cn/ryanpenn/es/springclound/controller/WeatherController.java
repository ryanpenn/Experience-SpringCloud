package cn.ryanpenn.es.springclound.controller;

import cn.ryanpenn.es.springclound.model.WeatherResponse;
import cn.ryanpenn.es.springclound.service.WeatherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * WeatherController
 *
 * @author pennryan
 */
@Api("天气服务API")
@RestController
@RequestMapping("api")
public class WeatherController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    WeatherService weatherService;

    @ApiOperation(value = "根据城市代码查询天气", notes = "查询城市的天气数据")
    @ApiImplicitParam(name = "code", value = "城市代码", paramType = "path", required = true, dataType = "Integer")
    @RequestMapping(value = "/code/{code}", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    public WeatherResponse getByCityCode(@PathVariable int code) {
        logger.info("/code/" + code);
        return weatherService.getByCityCode(code);
    }

    @ApiOperation(value = "根据城市名称查询天气", notes = "查询城市的天气数据")
    @ApiImplicitParam(name = "name", value = "城市名称", paramType = "path", required = true, dataType = "String")
    @RequestMapping(value = "/name/{name}", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    public WeatherResponse getByCityName(@PathVariable String name) {
        logger.info("/name/" + name);
        return weatherService.getByCityName(name);
    }

    @ApiOperation(value = "根据城市名称查询天气", notes = "查询城市的天气数据")
    @ApiImplicitParam(name = "name", value = "城市名称", paramType = "query", required = true, dataType = "String")
    @RequestMapping(value = "/city", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public WeatherResponse getByPost(@RequestParam String name) {
        logger.info("/city?name=" + name);
        return weatherService.getByCityName(name);
    }
}
