package cn.ryanpenn.es.springclound.service;

import cn.ryanpenn.es.springclound.model.WeatherResponse;

/**
 * WeatherService
 *
 * @author pennryan
 */
public interface WeatherService {

    /**
     * 根据城市代码查询天气
     * @param code 城市代码
     * @return 天气数据
     */
    WeatherResponse getByCityCode(int code);

    /**
     * 根据城市名称查询天气
     * @param name 城市名称
     * @return 天气数据
     */
    WeatherResponse getByCityName(String name);
}
