package cn.ryanpenn.es.springclound.controller;

import cn.ryanpenn.es.springclound.service.CityService;
import cn.ryanpenn.es.springclound.service.WeatherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

/**
 * WeatherReportController
 *
 * @author pennryan
 */
@ApiIgnore
@Controller
@RequestMapping("weather")
public class WeatherReportController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    WeatherService weatherService;

    @Autowired
    CityService cityService;

    /**
     * 天气预报页面
     * @param code 城市代码,如:101010100 北京
     * @param model
     * @return
     */
    @RequestMapping(value = "/{code}", method = RequestMethod.GET)
    public ModelAndView index(@PathVariable String code, Model model) {
        model.addAttribute("cityCode", code);
        model.addAttribute("cityList", cityService.getCityData());
        model.addAttribute("weather", weatherService.getByCityCode(code).getData());
        return new ModelAndView("weather", "report", model);
    }

}
