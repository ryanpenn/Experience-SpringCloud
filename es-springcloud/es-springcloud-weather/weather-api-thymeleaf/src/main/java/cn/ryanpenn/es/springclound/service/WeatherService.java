package cn.ryanpenn.es.springclound.service;

import cn.ryanpenn.es.springclound.model.WeatherResponse;

/**
 * WeatherService
 *
 * @author pennryan
 */
public interface WeatherService {

    /**
     * 根据城市代码查询天气
     * @param code 城市代码
     * @return 天气数据
     */
    WeatherResponse getByCityCode(String code);

    /**
     * 根据城市名称查询天气
     * @param name 城市名称
     * @return 天气数据
     */
    WeatherResponse getByCityName(String name);

    /**
     * 同步城市的天气数据
     * @param code 城市代码
     */
    void syncDataByCityId(String code);
}
