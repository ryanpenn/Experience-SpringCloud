package cn.ryanpenn.es.springclound.model;

/**
 * WeatherResponse
 *
 * @author pennryan
 */
public class WeatherResponse {

    Weather data;
    Integer status;
    String desc;

    public WeatherResponse() {
    }

    public WeatherResponse(Weather data, Integer status, String desc) {
        this.data = data;
        this.status = status;
        this.desc = desc;
    }

    public Weather getData() {
        return data;
    }

    public void setData(Weather data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
