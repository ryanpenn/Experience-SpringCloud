var app = new Vue({
    el: '#app',
    data: {
        title: '城市列表',
        report: {
            cityCode: -1,
            cityList: [],
            weather: null
        }
    },
    methods: {
        getCityList: function () {
            var that = this;
            $.ajax({
                url: "/api/citylist",
                success: function (result) {
                    app.$data.cityCode = result[0].code;
                    app.$data.report.cityList = result;
                    that.getWeather(result[0].code);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest.status);
                    console.log(XMLHttpRequest.readyState);
                    console.log(textStatus);
                }
            });
        },
        getWeather: function (cityCode) {
            var that = this;
            $.ajax({
                url: "/api/code/" + cityCode,
                success: function (result) {
                    app.$data.report.weather = result.data;
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest.status);
                    console.log(XMLHttpRequest.readyState);
                    console.log(textStatus);
                }
            });
        },
        // 切换城市
        chooseCity: function (e) {
            console.log(e.target.value);
            this.getWeather(e.target.value);
        },
        init: function () {
            this.getCityList();
        }
    }
});